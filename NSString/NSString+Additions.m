//
//  NSString+Additions.m
//
//  Created by Angey on 24.03.13.
//  Copyright (c) 2013 angey. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)firstLetter
{
    if (!self.length || self.length == 1)
        return self;
    
    return [self substringToIndex:1];
}

+ (NSString*)randomStringOfLength:(NSInteger)length
{
    NSString *random_chars = @"abcdefghijklmnopqrstuvwxyz";
    
    NSMutableString *tmp = [NSMutableString string];
    for (int j = 0; j < length; j++) {
        NSString *randomChar = [random_chars substringWithRange:NSMakeRange(arc4random() % 25, 1)];
        [tmp appendString:randomChar];
    }
    
    return [NSString stringWithString:tmp];
}

- (NSString*)toCamelCase:(NSString*)separator
{
    NSArray *elements = [self componentsSeparatedByString:separator];
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSString *string in elements){
        [resultArray addObject:[string stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                   withString:[[string substringToIndex:1] capitalizedString]]];
    }
    return [resultArray componentsJoinedByString:@""];
}

- (NSString*)base64String
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    return [data base64EncodedStringWithOptions:0];
}

- (NSString *)urlencode 
{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' || 
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (NSNumber*)numberValue
{
    return @([self integerValue]);
}

- (BOOL)containsString:(NSString*)substring
{
    return [self rangeOfString:substring].location != NSNotFound;
}

- (NSString*)strimmedWhiteSpacesString
{
    NSString *trimmedString = [self copy];
    
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }    
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }   
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }   
    while ([trimmedString rangeOfString:@" \n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@" \n" withString:@"\n"];
    }
    while ([trimmedString rangeOfString:@"\n "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n " withString:@"\n"];
    }   
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }    
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }   
    while ([trimmedString rangeOfString:@"  "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    while ([trimmedString rangeOfString:@"\n\n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }   
    while ([trimmedString rangeOfString:@" \n"].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@" \n" withString:@"\n"];
    }
    while ([trimmedString rangeOfString:@"\n "].location != NSNotFound) {
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@"\n " withString:@"\n"];
    } 
    
    return trimmedString;
}


@end
