//
//  NSString+Hash.h
//  template-project
//
//  Created by tyler on 03.09.12.
//  Copyright (c) 2012 DigiPeople Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Hash)

- (NSString *)md5Hash;

- (NSData *)HMACSHA1:(NSString *)key;

@end
