//
//  NSString+Additions.h
//
//  Created by Angey on 24.03.13.
//  Copyright (c) 2013 angey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)firstLetter;

+ (NSString*)randomStringOfLength:(NSInteger)length;

- (NSString*)toCamelCase:(NSString*)separator;

- (NSString*)base64String;

- (NSString *)urlencode;

- (NSNumber*)numberValue;

- (BOOL)containsString:(NSString*)substring;

- (NSString*)strimmedWhiteSpacesString;

@end
