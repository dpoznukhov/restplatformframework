//
//  InstagramSDK.m
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import "RPFBaseSDK.h"

#import "RPFGetByUrlCommand.h"

@implementation RPFBaseSDK

+ (Class)queueClass
{
    return [RPFCommandsQueue class];
}

- (id)init
{
    self = [super init];
    if (self){
        self.commandsQueue = [[RPFCommandsQueue alloc ] init];
#ifdef DEFAUL_PARALLEL_NETWORK_COMMANDS_COUNT
        self.commandsQueue.maximumParallelCommandsCount = DEFAUL_PARALLEL_NETWORK_COMMANDS_COUNT;
#else
        self.commandsQueue.maximumParallelCommandsCount = 5;
#endif
        self.commandsQueue.delegate = self;
    }
    return self;
}

- (void)setProgressBlock:(ProgressBlock)progressBlock
{
    self.commandsQueue.progressBlock = progressBlock;
}

- (void)setCompletionBlock:(CompletionBlock)completionBlock
{
    self.commandsQueue.completionBlock = completionBlock;
}

- (void)setMaximumParallelCommandsCount:(NSUInteger)count
{
    self.commandsQueue.maximumParallelCommandsCount = count;
}

- (void)addCommandWithClass:(Class)className userId:(NSString *)userId completion:(DictionaryResponseBlock)completionBlock
{
    RPFAbstractCommand *command = [[className alloc] init];
    command.mainParameterID = userId;
    command.completionBlock = completionBlock;
    
    command.delegate = self.commandsQueue;
    [command initialize];
    
    [self.commandsQueue addCommand:command];
}

- (void)downloadFileWithId:(NSString*)fileID completion:(DictionaryResponseBlock)completionBlock 
{
    if (fileID == nil)
        return;
    __weak RPFBaseSDK *weak_self = self;
    [self.commandsQueue.internalOperationsQueue addOperationWithBlock:^{
        RPFDownloadFileWithUrlCommand *command = [weak_self.commandsQueue findCommandWithMainParameter:fileID];
        if (command != nil){
            command.completionBlock = completionBlock;
        }
        else {
            [weak_self addCommandWithClass:[RPFDownloadFileWithUrlCommand class] userId:fileID completion:completionBlock]; 
        }
    }];
}

- (void)stopDownloadingOfFileWithId:(NSString*)fileId
{
    [self.commandsQueue stopDownloadingOfFileWithId:fileId];
}

- (void)increaseDownloadPriorityOfFileWithId:(NSString*)fileId
{
    [self.commandsQueue increaseDownloadPriorityOfFileWithId:fileId];
}
- (void)pauseDownloadingOfFileWithId:(NSString*)fileId
{
    [self.commandsQueue pauseDownloadingOfFileWithId:fileId];
}

- (void)stopCommandsWithMarker:(id)marker
{
    [self.commandsQueue stopCommandsWithMarker:marker];
}


- (void)getByUrl:(NSString*)url completion:(DictionaryResponseBlock)completionBlock
{
    [self addCommandWithClass:[RPFGetByUrlCommand class] userId:url completion:completionBlock];
}


@end
