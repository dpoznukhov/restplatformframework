//
//  JournalCommandsQueue.h
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "RPFAbstractCommand.h"
#import "RPFDownloadFileWithUrlCommand.h"



typedef void (^ProgressBlock)(NSString *fileId, int64_t totalSendSize, int64_t expectSendSize);
typedef void (^CompletionBlock)(NSString *fileId, NSError *error);


@interface RPFCommandsQueue : NSObject <RPFCommandDelegate, NSURLSessionTaskDelegate>

@property (assign)NSUInteger maximumParallelCommandsCount;
@property (nonatomic, strong)NSMutableSet *downloadTasksSet;

@property (nonatomic, weak)id delegate;

- (void)addCommand:(RPFAbstractCommand*)command;

@property (nonatomic, strong)ProgressBlock progressBlock;
@property (nonatomic, strong)CompletionBlock completionBlock;
@property (nonatomic, strong)NSOperationQueue *backgroundQueue;

- (void)stopDownloadingOfFileWithId:(NSString*)fileId;
- (void)increaseDownloadPriorityOfFileWithId:(NSString*)fileId;
- (void)pauseDownloadingOfFileWithId:(NSString*)fileId;

- (void)stopCommandsWithMarker:(id)marker;



- (RPFAbstractCommand*)findCommandWithMainParameter:(NSString*)mainParameterID;

@property (nonatomic, strong)NSMutableArray *commandsArray;
- (NSUInteger)currentRunnedDataCommandsCount;

@property (nonatomic, strong)NSOperationQueue *internalOperationsQueue;

@end




