//
//  JournalSDKDelegate.h
//  JournalProject
//
//  Created by Dmitriy Poznuhov on 22/04/14.
//  Copyright (c) 2014 Dmitriy Poznuhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPFAbstractCommand.h"

@protocol RPFBaseSDKDelegate <NSObject>

- (void)downloadFileWithId:(NSString*)fileID completion:(DictionaryResponseBlock)completionBlock;

- (void)getByUrl:(NSString*)url completion:(DictionaryResponseBlock)completionBlock;

@end
