//
//  JournalSDK.h
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPFCommandsQueue.h"
#import "RPFBaseSDKDelegate.h"

@interface RPFBaseSDK : NSObject <RPFBaseSDKDelegate>

- (void)setProgressBlock:(ProgressBlock)progressBlock;
- (void)setCompletionBlock:(CompletionBlock)completionBlock;

@property (nonatomic, strong)RPFCommandsQueue *commandsQueue;

- (void)setMaximumParallelCommandsCount:(NSUInteger)count;
- (void)stopDownloadingOfFileWithId:(NSString*)fileId;
- (void)increaseDownloadPriorityOfFileWithId:(NSString*)fileId;
- (void)pauseDownloadingOfFileWithId:(NSString*)fileId;

- (void)stopCommandsWithMarker:(id)marker;

- (void)addCommandWithClass:(Class)className userId:(NSString *)userId completion:(DictionaryResponseBlock)completionBlock;

@end
