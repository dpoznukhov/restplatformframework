//
//  RPFGetByUrlCommand.h
//  GitHubClient
//
//  Created by Dmitriy Poznuhov on 20/01/16.
//  Copyright © 2016 Dmitriy Poznuhov. All rights reserved.
//

#import "RPFAbstractCommand.h"

@interface RPFGetByUrlCommand : RPFAbstractCommand

@end
