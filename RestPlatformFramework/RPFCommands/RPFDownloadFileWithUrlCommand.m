//
//  DownloadFileWithUrlCommand.m
//  JournalProject
//
//  Created by Dmitriy Poznuhov on 30/04/14.
//  Copyright (c) 2014 Dmitriy Poznuhov. All rights reserved.
//

#import "RPFDownloadFileWithUrlCommand.h"

@implementation RPFDownloadFileWithUrlCommand

- (void)initialize
{
    [self createURLRequestWithURLString:self.mainParameterID parameters:@{}];
        
    [self createDownloadTask];
    
    NSLog(@"download create");
}

- (void)dealloc
{
    NSLog(@"download dealloc");
}

@end
