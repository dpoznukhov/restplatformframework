//
//  DownloadFileWithUrlCommand.h
//  JournalProject
//
//  Created by Dmitriy Poznuhov on 30/04/14.
//  Copyright (c) 2014 Dmitriy Poznuhov. All rights reserved.
//

#import "RPFAbstractCommand.h"

@interface RPFDownloadFileWithUrlCommand : RPFAbstractCommand

@end
