//
//  AbstractJournalCommand.m
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import "RPFAbstractCommand.h"
#import "NSString+Additions.h"

@implementation RPFAbstractCommand

- (instancetype)init
{
    self = [super init];
    if (self){
        self.isRuned = NO;
        self.count = 0;
#ifdef DEFAUL_COMMAND_REPEAT_COUNT
        self.maxCount = DEFAUL_COMMAND_REPEAT_COUNT;
#else
        self.maxCount = 1;
#endif
    }
    
    return self;
}

- (void)initialize
{
    
}

- (void)execute
{        
    self.isRuned = YES;
    self.count++;
    [self.task resume];
}

#pragma mark - creating request

- (void)createURLRequestWithURLString:(NSString*)urlString parameters:(NSDictionary*)parameters
{
    NSMutableString *parametersString = [[NSMutableString alloc] init];
    int i = 0;
    for (NSString *key in parameters.allKeys){        
        
        if (![parameters[key] isKindOfClass:[NSString class]]){
            int g = 0;
        }
        
        if ([parameters[key] length] == 0)
            continue;
        [parametersString appendFormat:@"%@=%@", key, [parameters[key] urlencode]];
        ++i;
        
        if (parameters.allKeys.count != i){
            [parametersString appendFormat:@"&"];
        }
    }
    if (parametersString.length > 0)
        urlString = [urlString stringByAppendingFormat:@"?%@", parametersString];
    
    
    self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
}

- (void)createPostUrlRequestWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters
{
    [self createURLRequestWithURLString:urlString parameters:@{}];    
    [self.request setHTTPMethod:@"POST"];
    [self.request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSMutableString *parametersString = [[NSMutableString alloc] init];
    if (parameters){
        int i = 0;
        for (NSString *key in parameters.allKeys){
            
            if (![parameters[key] isKindOfClass:[NSString class]]){
                int g = 0;
            }
            
            [parametersString appendFormat:@"%@=%@", key, [parameters[key] urlencode]];
            ++i;
            
            if (parameters.allKeys.count != i){
                [parametersString appendFormat:@"&"];
            }
        }
    }
    [self.request setHTTPBody:[parametersString dataUsingEncoding:NSUTF8StringEncoding]];
}


- (void)createPutUrlRequestWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters
{
    [self createPostUrlRequestWithUrlString:urlString parameters:parameters];
    [self.request setHTTPMethod:@"PUT"];
}

- (void)createDeleteUrlCommandWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters
{
    [self createPostUrlRequestWithUrlString:urlString parameters:parameters];
    [self.request setHTTPMethod:@"DELETE"];
}

#pragma mark - json parsing

- (id)parseData:(NSData *)data
{
    if (data){
        NSError *error;
        id result = [NSJSONSerialization JSONObjectWithData:data
                                                    options:NSJSONReadingMutableContainers |
                     NSJSONReadingMutableLeaves |
                     NSJSONReadingAllowFragments
                                                      error:&error];
        return result;
    }
    return nil;
}

#pragma mark - creating task

- (void)createDataTask
{
    __weak RPFAbstractCommand *weak_self = self;
    
    self.task = [[self.delegate sharedDataSession] dataTaskWithRequest:self.request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        __block NSError *connectionError = error;
        if (weak_self.completionBlock == nil)
            return;
        
        NSHTTPURLResponse *resp = (NSHTTPURLResponse*)response;
        
        NSDictionary *headers = resp.allHeaderFields;
        
        id json = [weak_self parseData:data];
        
        NSString *temp = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [[weak_self.delegate backgroundQueue] addOperationWithBlock:^{
            if (connectionError == nil && json == nil)
                connectionError = [NSError errorWithDomain:response.URL.absoluteString code:1 userInfo:@{@"description": response.description}];
            
            weak_self.isRuned = NO;
            if (weak_self.completionBlock != nil)
                weak_self.isRuned = weak_self.completionBlock(headers, json, connectionError) &&
                [weak_self.request.HTTPMethod isEqualToString:@"GET"];
            
            NSString *a = temp;//to retain
            if (weak_self.isRuned){
                [weak_self createURLRequestWithURLString:json[@"pagination"][@"next_url"] parameters:@{}];
                
                [weak_self createDataTask];
                [weak_self execute];    
            }
            
            else if ([weak_self.delegate respondsToSelector:@selector(RPFCommand:finishedWithError:)])
                [weak_self.delegate RPFCommand:weak_self finishedWithError:connectionError];
        }];
    }];
}

- (void)createDownloadTask
{
    __weak RPFAbstractCommand *weak_self = self;
    [[self.delegate internalOperationsQueue] addOperationWithBlock:^{
        NSURLSessionDownloadTask *task = [[weak_self.delegate sharedDownloadSession] downloadTaskWithRequest:weak_self.request];
        
        CFUUIDRef udid = CFUUIDCreate(NULL);
        NSString *udidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
        
        [task setTaskDescription:udidString];
        weak_self.task = task;
        [weak_self.delegate addDownloadURLTask:weak_self.task];
    }];    
    
    
}

@end
