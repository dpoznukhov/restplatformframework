//
//  RPFGetByUrlCommand.m
//  GitHubClient
//
//  Created by Dmitriy Poznuhov on 20/01/16.
//  Copyright © 2016 Dmitriy Poznuhov. All rights reserved.
//

#import "RPFGetByUrlCommand.h"

@implementation RPFGetByUrlCommand

- (void)initialize
{
    [self createURLRequestWithURLString:self.mainParameterID parameters:nil];
    
    [self createDataTask];
}

@end
