//
//  AbstractJournalCommand.h
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef BOOL (^DictionaryResponseBlock)(NSDictionary *headers, NSDictionary *data, NSError *error);


@protocol RPFCommandDelegate;

@interface RPFAbstractCommand : NSObject 

@property (assign)BOOL isRuned;
@property (nonatomic, weak) id<RPFCommandDelegate> delegate;
@property (nonatomic, strong) DictionaryResponseBlock completionBlock;


@property (nonatomic, strong)NSMutableURLRequest *request;
@property (nonatomic, strong)NSString *mainParameterID;

@property (assign)NSInteger count;
@property (assign)NSInteger maxCount;

@property (nonatomic, strong)NSURLSessionTask *task;

- (void)initialize;//call after all seters

- (void)execute;
- (void)createURLRequestWithURLString:(NSString*)urlString parameters:(NSDictionary*)parameters;
- (void)createPostUrlRequestWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters;
- (void)createPutUrlRequestWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters;
- (void)createDeleteUrlCommandWithUrlString:(NSString*)urlString parameters:(NSDictionary*)parameters;

- (void)createDataTask;
- (void)createDownloadTask;

@property (nonatomic, strong)id marker;

@end

@protocol RPFCommandDelegate <NSObject>

- (void)RPFCommand:(RPFAbstractCommand*)command finishedWithError:(NSError*)error;

- (NSURLSession*)sharedDataSession;
- (NSURLSession*)sharedDownloadSession;
- (void)addDownloadURLTask:(id)task;

- (NSOperationQueue*)backgroundQueue;

- (NSOperationQueue*)internalOperationsQueue;

- (id)sdk;

@end