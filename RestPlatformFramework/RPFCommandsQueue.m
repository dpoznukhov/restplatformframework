//
//  JournalCommandsQueue.m
//  Collage
//
//  Created by Dmitry on 3/12/14.
//  Copyright (c) 2014 Dmitry. All rights reserved.
//

#import "RPFCommandsQueue.h"
#import "RPFBasicDefines.h"

@interface RPFCommandsQueue ()

//@property (nonatomic, strong)NSMutableDictionary *download

@end


@implementation RPFCommandsQueue
{
    NSOperationQueue *_backgroundQueue;
}

- (instancetype)init
{
    self = [super init];
    if (self){
        self.maximumParallelCommandsCount = DEFAUL_PARALLEL_NETWORK_COMMANDS_COUNT;
        self.commandsArray = [NSMutableArray array];
        
        self.downloadTasksSet = [NSMutableSet new];
        
        self.internalOperationsQueue = [[NSOperationQueue alloc] init];
        [self.internalOperationsQueue setName:@"JournalSDK_Queue"];
        [self.internalOperationsQueue setMaxConcurrentOperationCount:1];
    }
    
    return self;
}

- (void)setCompletionBlock:(CompletionBlock)completionBlock
{
    _completionBlock = completionBlock;
        
    [self sharedDataSession];
    [self sharedDownloadSession];
}

- (void)addCommand:(RPFAbstractCommand*)command
{
    __weak RPFCommandsQueue *weak_self = self;    
    [self.internalOperationsQueue addOperationWithBlock:^{        
        [weak_self.commandsArray addObject:command];
        command.delegate = weak_self;        
        [weak_self updateCommands];
    }];
}

- (NSUInteger)currentRunnedDataCommandsCount
{
    NSUInteger count = 0;
    for (RPFAbstractCommand *command in [self.commandsArray copy]){
        if ([command isKindOfClass:[RPFDownloadFileWithUrlCommand class]])
            continue;
        
        if (command.isRuned)
            count++;
    }
    return count;
}

- (NSUInteger)currentRunnedDownloadCommandsCount
{
    NSUInteger count = 0;
    for (RPFAbstractCommand *command in [self.commandsArray copy]){
        if (![command isKindOfClass:[RPFDownloadFileWithUrlCommand class]])
            continue;
        
        if (command.isRuned)
            count++;
    }
    return count;
}

- (void)updateCommands
{
    __weak RPFCommandsQueue *weak_self = self;
    for (RPFAbstractCommand *command in [weak_self.commandsArray copy]){ 
        if ([command isKindOfClass:[RPFDownloadFileWithUrlCommand class]])
            continue;
        if (weak_self.currentRunnedDataCommandsCount >= weak_self.maximumParallelCommandsCount)
            break;
        
        if (!command.isRuned){
            [command execute];
        }
    }
    for (RPFAbstractCommand *command in [weak_self.commandsArray copy]){  
        if (![command isKindOfClass:[RPFDownloadFileWithUrlCommand class]])
            continue;
        if (weak_self.currentRunnedDownloadCommandsCount >= DEFAUL_PARALLEL_DOWNLOAD_COMMANDS_COUNT)
            break;
        
        if (!command.isRuned){
            [command execute];
        }
    }
}

- (void)stopDownloadingOfFileWithId:(NSString*)fileId
{
    __weak RPFCommandsQueue *weak_self = self;
    [self.internalOperationsQueue setSuspended:YES];
    NSURLSessionDownloadTask *findedTask;
    for (NSURLSessionDownloadTask *task in [weak_self.downloadTasksSet copy]){
        if ([[task.currentRequest.URL.absoluteString componentsSeparatedByString:@"?"].firstObject isEqualToString:fileId]){
            findedTask = task;
            break;
        }
    }
    if (findedTask != nil){
        [findedTask cancel];
        [weak_self.downloadTasksSet removeObject:findedTask];
        
        RPFAbstractCommand *findedCommand;
        DictionaryResponseBlock findedCommandCompletionBlock;
        for (RPFAbstractCommand *command in [weak_self.commandsArray copy]){
            if (command.task == findedTask){
                findedCommand = command;
                findedCommandCompletionBlock = command.completionBlock;
                command.completionBlock = nil;
                break;
            }
        }
        if (findedCommandCompletionBlock != nil)
            findedCommandCompletionBlock(nil, nil, [NSError errorWithDomain:@"RPFCommandsQueue" code:1 userInfo:nil]);
        if (findedCommand != nil)
            [weak_self.commandsArray removeObject:findedCommand];
    }        
    [weak_self updateCommands];
    [self.internalOperationsQueue setSuspended:NO];
}

- (void)increaseDownloadPriorityOfFileWithId:(NSString*)fileId
{
    static float priority;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        priority = 0.0;
    });
    priority += 0.01;
    __weak RPFCommandsQueue *weak_self = self;
    NSURLSessionDownloadTask *findedTask;
    for (NSURLSessionDownloadTask *task in [weak_self.downloadTasksSet copy]){
        if ([[task.currentRequest.URL.absoluteString componentsSeparatedByString:@"?"].firstObject isEqualToString:fileId]){
            findedTask = task;
            break;
        }
    }
    if (findedTask != nil){        
//        [findedTask setPriority:priority];
    }        
}

- (void)pauseDownloadingOfFileWithId:(NSString*)fileId
{
    __weak RPFCommandsQueue *weak_self = self;
    NSURLSessionDownloadTask *findedTask;
    for (NSURLSessionDownloadTask *task in [weak_self.downloadTasksSet copy]){
        if ([[task.currentRequest.URL.absoluteString componentsSeparatedByString:@"?"].firstObject isEqualToString:fileId]){
            findedTask = task;
            break;
        }
    }
    if (findedTask != nil){  
    //    [findedTask setPriority:0.0];
    }  
}

- (void)stopCommandsWithMarker:(id)marker
{
    __weak RPFCommandsQueue *weak_self = self;
    [self.internalOperationsQueue setSuspended:YES];
    NSMutableArray *findedCommands = [NSMutableArray array];
    for (RPFAbstractCommand *command in [weak_self.commandsArray copy]){
        if ([command.marker isEqual:marker]){
            [findedCommands addObject:command];
            [command.task cancel];
            command.completionBlock = nil;
        }
    }
    if (findedCommands.count > 0){
        [weak_self.commandsArray removeObjectsInArray:findedCommands];
    }
    [weak_self updateCommands];
    [self.internalOperationsQueue setSuspended:NO];
}

#pragma mark - JournalCommandDelegate methods

- (void)RPFCommand:(RPFAbstractCommand*)command finishedWithError:(NSError*)error
{
    __weak RPFCommandsQueue *weak_self = self;
    __strong RPFAbstractCommand *command_strong = command;
    [self.internalOperationsQueue addOperationWithBlock:^{
        if (command.task != nil){
            [weak_self.downloadTasksSet removeObject:command.task];
        }
        [weak_self.commandsArray removeObject:command];
        
        if (error != nil){
            if (command.count < command.maxCount || command.maxCount == 0){
                [command_strong initialize];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(COMMAND_REPEAT_DELAY_SEC * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weak_self addCommand:command_strong];
                });
            }
            else {
                if ([command isKindOfClass:[RPFDownloadFileWithUrlCommand class]])
                    weak_self.completionBlock(command.task.currentRequest.URL.absoluteString, error);
                [weak_self updateCommands];
            }
        }
        else {
            [weak_self updateCommands];
        }
    }];
}

- (NSURLSession*)sharedDataSession
{
    __weak RPFCommandsQueue *weak_self = self;
    static NSURLSession *session;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//        sessionConfig.timeoutIntervalForResource = 0;
        sessionConfig.timeoutIntervalForRequest = 20;
//        [sessionConfig setHTTPMaximumConnectionsPerHost:20];
        session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:weak_self delegateQueue:weak_self.internalOperationsQueue];
    });
    
    return session;
}

- (NSURLSession*)sharedDownloadSession
{
    __weak RPFCommandsQueue *weak_self = self;
    static NSURLSession *session;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *config;
        if (IS_IOS_7)
            config = [NSURLSessionConfiguration backgroundSessionConfiguration:[self description]];
        else 
            config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[self description]];
            
//        config.timeoutIntervalForResource = 0;
//        config.timeoutIntervalForRequest = 0;
//        [config setHTTPMaximumConnectionsPerHost:20];
        [config setTimeoutIntervalForRequest:1000000.0];
        [config setTimeoutIntervalForResource:100.0];
        session = [NSURLSession sessionWithConfiguration:config delegate:weak_self delegateQueue:weak_self.internalOperationsQueue];
    });
    
    return session;
}

- (void)setBackgroundQueue:(NSOperationQueue *)backgroundQueue
{
    backgroundQueue.maxConcurrentOperationCount = 1;
    _backgroundQueue = backgroundQueue;    
}

- (NSOperationQueue *)backgroundQueue
{
    if (_backgroundQueue == nil){
        _backgroundQueue = [[NSOperationQueue alloc] init];
        _backgroundQueue.name = @"RPFOperationQueue";
        _backgroundQueue.maxConcurrentOperationCount = 1;
    }
    return _backgroundQueue;
}

- (void)addDownloadURLTask:(id)task
{
    if (task == nil)
        return;
    __weak RPFCommandsQueue *weak_self = self;
    [self.internalOperationsQueue addOperationWithBlock:^{
        [weak_self.downloadTasksSet addObject:task];
    }];
}

#pragma mark - NSURLSession delegate methods

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    __weak RPFCommandsQueue *weak_self = self;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    NSArray *urls = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [[urls objectAtIndex:0] URLByAppendingPathComponent:@"Files"];
    //    NSURL *documentsDirectory = [NSURL URLWithString:[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Files/"]];
    
    NSError *fileManagerError;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory.path])
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory.path
                                  withIntermediateDirectories:NO 
                                                   attributes:nil 
                                                        error:&fileManagerError]; //Create folder    
    
    NSURL *originalUrl = [NSURL URLWithString:[downloadTask.currentRequest.URL.absoluteURL lastPathComponent]];
    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:[originalUrl lastPathComponent]];
    
    [fileManager removeItemAtURL:destinationUrl error:NULL];
    //ключевая  строчка!
    [fileManager moveItemAtURL:location toURL:destinationUrl error:&fileManagerError];
    RPFAbstractCommand *findedCommand;
    for (RPFAbstractCommand *command in [weak_self.commandsArray copy])
        if (command.task == downloadTask)
            findedCommand = command;  
    
    [self.backgroundQueue addOperationWithBlock:^{
        weak_self.completionBlock([[downloadTask.currentRequest.URL.absoluteString componentsSeparatedByString:@"?"] firstObject], nil);  
        
        if (findedCommand.completionBlock != nil){
            findedCommand.completionBlock(nil, @{@"status" : @"ok"}, nil);
        }
        
        [weak_self RPFCommand:findedCommand finishedWithError:nil];
    }];
}

- (void)URLSession:(NSURLSession *)session 
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{       
    __weak RPFCommandsQueue *weak_self = self;
    
    [self.internalOperationsQueue addOperationWithBlock:^{
        [self.backgroundQueue addOperationWithBlock:^{
            
            weak_self.progressBlock([[downloadTask.currentRequest.URL.absoluteString componentsSeparatedByString:@"?"] firstObject],
                                    downloadTask.countOfBytesReceived, downloadTask.countOfBytesExpectedToReceive);
            
            [weak_self.internalOperationsQueue addOperationWithBlock:^{
                [weak_self.downloadTasksSet addObject:downloadTask];
            }];            
        }];            
    }];
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error == nil){
        return;
    }   
    NSLog(@"URLSession didCompleteWithError %@", [[[task currentRequest] URL] absoluteString]);
    __weak RPFCommandsQueue *weak_self = self;
    [self.internalOperationsQueue addOperationWithBlock:^{        
        RPFAbstractCommand *findedCommand;
        for (RPFAbstractCommand *command in [weak_self.commandsArray copy])
            if (command.task == task)
                findedCommand = command;  
                
        if (findedCommand != nil){  
            [weak_self RPFCommand:findedCommand finishedWithError:error];
        }
    }];
}

- (RPFAbstractCommand*)findCommandWithMainParameter:(NSString*)mainParameterID
{
    for (RPFAbstractCommand *command in [self.commandsArray copy]){
        if ([command.mainParameterID isEqualToString:mainParameterID])
            return command;
    }
    return nil;
}

- (id)sdk
{
    return self.delegate;
}

@end
