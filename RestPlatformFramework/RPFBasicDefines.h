//
//  BasicDefines.h
//  RestPlatformFramework
//
//  Created by Dmitriy Poznuhov on 09/01/15.
//  Copyright (c) 2015 Dmitriy Poznuhov. All rights reserved.
//

#ifndef RestPlatformFramework_BasicDefines_h
#define RestPlatformFramework_BasicDefines_h


#define COMMAND_REPEAT_DELAY_SEC 2

#define DEFAUL_PARALLEL_NETWORK_COMMANDS_COUNT 5
#define DEFAUL_PARALLEL_DOWNLOAD_COMMANDS_COUNT 100000

#define IOS_VERSION                         [[[UIDevice currentDevice] systemVersion] floatValue]
#define IS_IOS_7                            (IOS_VERSION>=7.0&&IOS_VERSION<8.0)
#define IS_IOS_8                            (IOS_VERSION>=8.0&&IOS_VERSION<9.0)

#endif
