//
//  RestPlatformFramework.h
//  RestPlatformFramework
//
//  Created by Dmitriy Poznuhov on 12/12/14.
//  Copyright (c) 2014 Dmitriy Poznuhov. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RestPlatformFramework.
FOUNDATION_EXPORT double RestPlatformFrameworkVersionNumber;

//! Project version string for RestPlatformFramework.
FOUNDATION_EXPORT const unsigned char RestPlatformFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RestPlatformFramework/PublicHeader.h>

//
#import "RPFBaseSDK.h"